package com.example.tp2wine;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    WineDbHelper db;
    ListView listWine;
    List<String> listewines;
    List<Wine> wines = new ArrayList<Wine>();
   public SimpleCursorAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = new WineDbHelper(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.listWine = findViewById(R.id.listWine);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, WineActivity.class);

                MainActivity.this.startActivity(myIntent);

            }
        });

        listWine.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Wine wine = wines.get(position);

                Intent myIntent = new Intent(MainActivity.this, WineActivity.class);
                myIntent.putExtra("wine", wine);
                MainActivity.this.startActivity(myIntent);
            }
        });

        registerForContextMenu(listWine);



        db.populate();
        this.parstList();
        Cursor cursor=db.fetchAllWines();

        // Ajout de Simple Adapter
        adapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_list_item_2,
                cursor,
                new String[] { WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION },
                new int[] { android.R.id.text1,android.R.id.text2 },0
        );

        this.listWine.setAdapter(adapter);



    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch(item.getItemId()) {

            case R.id.delete: Cursor iteme = (Cursor)listWine.getItemAtPosition(info.position);

                Wine wine=db.cursorToWine(iteme);
                db.deleteWine(iteme);

                Toast.makeText(this,"Un vin a été supprimer",Toast.LENGTH_SHORT).show();
                adapter.changeCursor(db.fetchAllWines());

                adapter.notifyDataSetChanged(); return true;
            default:return super.onOptionsItemSelected(item);

        }
    }

    private void parstList() {
         listewines = new ArrayList<String>();

        Cursor cursor = db.fetchAllWines();


        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                Wine wine = db.cursorToWine(cursor);
                listewines.add(wine.getTitle());
                wines.add(wine);
                cursor.moveToNext();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
