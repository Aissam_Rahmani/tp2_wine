package com.example.tp2wine;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {



    private EditText editTitle;
    private EditText editWineRegion;
    private EditText editLoc;
    private EditText editClimate;
    private EditText editPlantedArea;
    private Button buttonSauvgarder;


    WineDbHelper db;

    Wine wine;
    Wine winerecu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Intent intent = getIntent();


        editTitle = findViewById(R.id.editTitle);
        editWineRegion = findViewById(R.id.editWineRegion);
        editLoc = findViewById(R.id.editLoc);
        editClimate = findViewById(R.id.editClimate);
        editPlantedArea = findViewById(R.id.editPlantedArea);
        buttonSauvgarder = findViewById(R.id.button);


        this.winerecu = (Wine) getIntent().getParcelableExtra("wine");

        if (winerecu != null) {


            this.wine = winerecu;

            editTitle.setText(this.wine.getTitle());
            editWineRegion.setText(this.wine.getRegion());
            editLoc.setText(this.wine.getLocalization());
            editClimate.setText(this.wine.getClimate());
            editPlantedArea.setText(this.wine.getPlantedArea());



        } else {

            this.wine = new Wine();
        }


        db = new WineDbHelper(this);
    }


    public void sauvegarder(View view) {

        String editTitleText = editTitle.getText().toString();
        String editWineRegionText = editWineRegion.getText().toString();
        String editLocText = editLoc.getText().toString();
        String editClimateText = editClimate.getText().toString();
        String editPlantedAreaText = editPlantedArea.getText().toString();

        if (editTitleText.isEmpty()) {



            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
            alertDialogBuilder.setTitle("Sauvegarde impossible");
            alertDialogBuilder.setMessage("Le nom du vin doit être non vide.");
            alertDialogBuilder.create();
            alertDialogBuilder.show();

        } else {

            this.wine.setTitle(editTitleText);
            this.wine.setRegion(editWineRegionText);
            this.wine.setLocalization(editLocText);
            this.wine.setClimate(editClimateText);
            this.wine.setPlantedArea(editPlantedAreaText);

            boolean inserer;

            if (this.winerecu != null) {
                inserer = db.updateWine(this.wine) > 0;
            } else {
                inserer = db.addWine(this.wine);
            }

            if (inserer == true) {

                Toast.makeText(WineActivity.this, "Nouveu wine Ajouter", Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(WineActivity.this, MainActivity.class);
                WineActivity.this.startActivity(myIntent);

            } else {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(WineActivity.this);
                alertDialogBuilder.setTitle("Ajout impossible");
                alertDialogBuilder.setMessage("Un vin portant le même nom existe déjà dans la base de données.");
                alertDialogBuilder.create();
                alertDialogBuilder.show();

            }
        }
    }


}
